import uuid

import boto3

import utils

ecs_client = boto3.client("ecs")
ssm_client = boto3.client("ssm")
cloudwatch_logs_client = boto3.client("logs")


GITLAB_RUNNER_HELPER_IMAGE = "gitlab/gitlab-runner-helper:x86_64-bleeding"
BUILD_WORKSPACE_VOLUME = "build-workspace"
BUILD_DIRECTORY = "/builds"


class RunnerJob(object):
    def __init__(self, runner_config, job_config):
        self.runner_config = runner_config
        self.job_config = job_config

        # This is used for randomness so that we can uniquely link a task to its logs and secrets
        # If we didn't want need to support running runners for different Gitlab instances in the same AWS account then
        # we could just use the CI_JOB_ID which would be unique for the job for that Gitlab instance.
        self.job_id = str(uuid.uuid4())

        self._task_arn = None

    def create_task_definition(self):
        job_secrets = self.put_secrets_in_parameter_store()

        # This is just an example to show that we can write data (eg clone a repo) in a container using the Gitlab
        # runner helper image and then access it in the job container specified in .gitlab-ci.yml
        # TODO: Clone the repo with the CI_REPOSITORY_URL environment variable and also handle caches and artifacts
        pre_job_container = {
            "name": "pre-job-helper",
            "image": GITLAB_RUNNER_HELPER_IMAGE,
            "commands": [
                "echo 'this is the pre-job-helper container' | tee pre-job-helper.out",
                "date",
                "sleep 5",
                "date",
            ],
            "environment_variables": self.job_config["variables"],
            "secrets": job_secrets,
        }
        pre_job_container_definition = self.create_container_definition(pre_job_container)

        job_container = {
            "name": "job",
            "image": self.job_config["image"],
            "commands": self.job_config["script_commands"],
            "environment_variables": self.job_config["variables"],
            "secrets": job_secrets,
            "depends_on": [{"containerName": "pre-job-helper", "condition": "SUCCESS"},],
        }
        job_container_definition = self.create_container_definition(job_container)

        # The after script needs to be able to run even if the before_script + script errors so it runs in a separate
        # container as soon as the main job exits. This should only be included if the job contains an after_script
        after_script_container = {
            "name": "after-script-job",
            "image": self.job_config["image"],
            "commands": ["echo 'this is the after-script-job container'", "date", "sleep 5", "date",],
            "environment_variables": self.job_config["variables"],
            "secrets": job_secrets,
            "depends_on": [{"containerName": "job", "condition": "COMPLETE"},],
        }
        after_script_container_definition = self.create_container_definition(after_script_container)

        # This is just an example to show that we can inspect the job status code so that we can conditionally handle
        # artifacts that need to be created on success or failure or always
        # TODO: Handle cache and conditionally create artifacts based on the job artifact config and the job status
        #  this should be a no-op container if there is no cache or artifact configuration for the job but still needs
        #  to exist so we have at least one essential container in the task
        check_job_exit_code_shell_command = """$(curl --silent ${ECS_CONTAINER_METADATA_URI}/task |
            jq '.Containers[] | select(.Name==\"job\").ExitCode')"""
        post_job_container = {
            "name": "post-job-helper",
            "image": GITLAB_RUNNER_HELPER_IMAGE,
            "commands": [
                "echo 'this is the post-job-helper container'",
                "date",
                "sleep 5",
                "date",
                "apk add --no-cache curl jq",
                f"echo job exit code: {check_job_exit_code_shell_command}",
            ],
            "environment_variables": self.job_config["variables"],
            "secrets": job_secrets,
            # Tasks must have at least one essential container and will exit if an essential container does
            "essential": True,
            "depends_on": [{"containerName": "after-script-job", "condition": "COMPLETE",},],
        }
        post_job_container_definition = self.create_container_definition(post_job_container)

        response = ecs_client.register_task_definition(
            # TODO: Maybe this should contain the date so we can automatically make old task definitions invalid later
            #   The only apparent problem here is the very large 1 million revisions per task definition family
            family=f"gitlab-ci-ecs-hack-example-{self.job_config['name']}",
            taskRoleArn=self.runner_config.get("task_role_arn", ""),
            executionRoleArn=self.runner_config["execution_role_arn"],
            # TODO: This should be configurable for EC2 based tasks.
            #  If so then the network configuration of run_task also needs to change appropriately
            networkMode="awsvpc",
            containerDefinitions=[
                pre_job_container_definition,
                job_container_definition,
                after_script_container_definition,
                post_job_container_definition,
            ],
            volumes=[{"name": BUILD_WORKSPACE_VOLUME},],
            requiresCompatibilities=["EC2", "FARGATE"],
            cpu=str(self.runner_config["task_cpu_limit"]),
            memory=str(self.runner_config["task_memory_limit"]),
            tags=utils.dict_to_list_of_key_value_dicts(
                {
                    "CI": True,
                    "CI_PROJECT_ID": self.job_config["variables"]["CI_PROJECT_ID"],
                    "CI_PROJECT_PATH": self.job_config["variables"]["CI_PROJECT_PATH"],
                },
                "tags",
            ),
        )

        return response["taskDefinition"]["taskDefinitionArn"]

    def put_secrets_in_parameter_store(self):
        """
        Temporarily stashes encrypted secrets in SSM's parameter store so that the task can fetch it at run time.
        Prevents the secrets being stored in the immutable task definition that is readable by ReadOnly roles.

        Assumes we have a separate list of secrets and variables so that only secrets are stored outside of the task
        definition. If this isn't true (quick glance at the custom executor docs suggests not) then we might need to
        work out what can be stored in the task definition and what needs to be stored securely.

        Unfortunately there are two user controlled variable types: the project/group variables which should be
        considered secrets and the job variables defined in .gitlab-ci.yml which are not secrets.
        Worst case scenario we just put non secret predefined variables in the task definition and everything else
        in the parameter store.
        """
        task_secrets = {}
        for job_secret in self.job_config["secrets"].items():
            value_from = self.put_secret_in_parameter_store(job_secret)
            task_secrets[job_secret[0]] = value_from

        return task_secrets

    def put_secret_in_parameter_store(self, job_secret):
        """
        :param job_secret: A tuple of the name and the value
        :return: The path to where it is stored in SSM parameter store
        """
        ssm_path = f"/gitlab/ci/{self.job_id}/{job_secret[0]}"

        response = ssm_client.put_parameter(
            Name=ssm_path,
            Value=job_secret[1],
            Description="Created by Gitlab CI ECS executor",
            Type="SecureString",
            KeyId=self.runner_config["secrets_kms_key"],
            Tier="Advanced",  # Required for the expiration policy
            Policies=utils.expire_parameter_in_one_hour(),
        )

        return ssm_path

    def create_container_definition(self, container):
        container_definition = {
            "name": container["name"],
            "image": container["image"],
            # TODO: Support pulling from private container registries that aren't ECR eg Gitlab Container Registry
            #   Unfortunately this will contain time limited secrets to be able to fetch the Docker image that are then
            #   stored in a readable state in the task definition
            # 'repositoryCredentials': {
            #     'credentialsParameter': 'string'
            # },
            # TODO: How does inter container networking work here? Will be needed for services
            #   Services aren't yet supported by custom executors.
            #   See https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4358
            # 'links': [
            #     'string',
            # ],
            "essential": container.get("essential", False),
            # TODO: Attempt to use Bash and fall back to POSIX sh if it's not present?
            "entryPoint": ["sh"],
            "command": utils.build_docker_command(container["commands"]),
            "environment": utils.dict_to_list_of_key_value_dicts(container["environment_variables"], "env"),
            "mountPoints": [
                {"sourceVolume": BUILD_WORKSPACE_VOLUME, "containerPath": BUILD_DIRECTORY, "readOnly": False},
            ],
            "secrets": utils.dict_to_list_of_key_value_dicts(container["secrets"], "secrets"),
            "workingDirectory": BUILD_DIRECTORY,
            "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-region": self.runner_config["aws_region"],
                    "awslogs-group": self.runner_config["aws_logs_group"],
                    "awslogs-stream-prefix": self.job_id,
                },
            },
        }

        if "depends_on" in container:
            container_definition["dependsOn"] = container["depends_on"]

        return container_definition

    def run_job(self):
        task_definition_arn = self.create_task_definition()
        response = ecs_client.run_task(
            taskDefinition=task_definition_arn,
            cluster=self.runner_config["cluster"],
            count=1,
            launchType=self.runner_config["launch_type"],
            networkConfiguration={
                "awsvpcConfiguration": {
                    "subnets": self.runner_config["subnet_ids"],
                    "securityGroups": [self.runner_config["security_group_id"]],
                }
            },
            # Could this be a link to the job instead? CI_JOB_URL. 36 character limit and only alphanum, - and _ :(
            startedBy=f"Gitlab CI - Job {self.job_config['variables']['CI_JOB_ID']}",
            propagateTags="TASK_DEFINITION",
        )

        self._task_arn = response["tasks"][0]["taskArn"]

    def job_task_still_running(self):
        response = ecs_client.describe_tasks(cluster=self.runner_config["cluster"], tasks=[self._task_arn])

        if response["tasks"][0].get("executionStoppedAt") is None:
            return True
        else:
            return False

    def get_logs(self):
        printed_events = []
        while self.job_task_still_running():
            response = cloudwatch_logs_client.filter_log_events(
                logGroupName=self.runner_config["aws_logs_group"],
                logStreamNamePrefix=self.job_id,
                # TODO: Handle pagination if log size is more than 1MB
                #  see https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/logs.html#CloudWatchLogs.Paginator.FilterLogEvents
            )
            events = response["events"]
            if events:
                for event in events:
                    # We need to track what things we've already printed because each call will return the full thing
                    if event["eventId"] not in printed_events:
                        printed_events.append(event["eventId"])
                        yield event["message"]

    def get_job_status(self):
        # TODO: Do we also need to check for runner failures if other steps fail?
        #   What about if it fails to provision?
        # This can only be called once the task has exited. We currently assume that the caller is going to call
        # get_job_logs and exhaust the generator first that will guarantee this
        response = ecs_client.describe_tasks(cluster=self.runner_config["cluster"], tasks=[self._task_arn])

        containers = response["tasks"][0]["containers"]
        container_statuses = {container["name"]: container["exitCode"] for container in containers}
        job_status = container_statuses["job"]

        return job_status
