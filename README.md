# Gitlab CI ECS Runner example

This forms an alternative approach to the official [Fargate Gitlab Runner](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate) that currently relies on SSH being enabled in the container.

The intention is to allow users of Gitlab CI to use any Docker image as they can with the Docker, Docker-Machine and Kubernetes executors.
It also intends to easily support EC2 because it should be trivial to support both EC2 and Fargate launch types.

There's also some [parallel work](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate/-/issues/12) on the official ECS Gitlab Runner project to try to remove the SSH dependency via a sidecar.
I feel like this is maybe more complex but it is definitely an interesting approach.

The code here is only meant to show the ECS side of things and how we can run a Gitlab CI workload in a native ECS way rather than relying on SSHing into the container.
It doesn't handle the actual Gitlab runner custom executor part and that is left as an exercise to be completed later, potentially as part of a rewrite (I would prefer this to be in Go but was quicker for me to hack together the example of how this all interacts with various AWS APIs in Python).

## General approach:
- Create a task definition on the fly with the necessary config to run the CI job
    - Each task will need to use container dependencies to stagger the pre job helper, the script container, the after script and finally the post job helper
    - Logs should be sent to Cloudwatch Logs
- Run the job
- Stream back the logs for the task
- Check the job container's status code so we can mark it as successful or not

Note that the [custom executor](https://docs.gitlab.com/runner/executors/custom.html#run) expects the following interactive stages:
- Config (optional) - This tells Gitlab some things that it can then pass into the following stages - probably not needed here
- Prepare (optional) - This could possibly be used for setting the task definition and secrets up
- Run (required) - This is called multiple times but in our case we need to fold this all into one block of the remaining actions above (run the task, get the logs, check the job output) because the task is not interactive once it is set
- Cleanup (optional) - We shouldn't need to do anything here. The logs should be using a log group that is user configurable to expire log streams after some period, the SSM parameters are automatically expired after 1 hour and task definitions can't be removed, only made inactive

I'm not 100% sure how we'll integrate this flow into what I have here but this at least demonstrates that it's possible to run Gitlab CI jobs on ECS (as either Fargate or EC2) in a native way without needing to resort to hacks such as SSHing into the build container.

## Running the examples

1. Configure AWS credentials (environment variables or credentials file etc) for the SDK
2. Install dependencies with [pipenv](https://pipenv.pypa.io/en/latest/):
  - `pipenv install`
3. Configure AWS account specific things in the `runner-config.yml` file
4. Run it
  - `pipenv run ./example.py`
5. After a few seconds you should see logs being streamed back from each job's ECS task followed by the status code of the job

## TODOs:
- Make services work somehow
    - This is currently blocked by https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4358
- Actually do something in the pre and post helper steps eg clone repo, create artifact
- after_script
- Support pulling from private Docker registries such as Gitlab Container Registry
- Be able to cancel running jobs (ie on ctrl+c in this example)
- Allow Docker building on EC2 instances (neither of these are available for Fargate)
  - Bind mount Docker socket
  - Allow privileged for Docker in Docker
- EC2 only network configuration
  - awsvpc requires either ENI trunking or an ENI per task which is a heavy resource cost for an instance
  - bridge or host networking should be fine as long as the EC2 instance can talk to the Gitlab instance (security groups etc)
    - `networkConfiguration` in the run_task API call should be omitted in this case
