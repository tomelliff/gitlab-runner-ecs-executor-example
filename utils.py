import json
from datetime import datetime, timedelta


def dict_to_list_of_key_value_dicts(objs, object_type):
    """
    The AWS API requires tags, environment variables and secrets to be a list of dicts
    with the key and value explicitly broken out instead of a simpler dict :(

    environment variables use name: value
    secrets use name: valueFrom:
    tags use key: value
    """
    if object_type == "env":
        k = "name"
        v = "value"
    elif object_type == "secrets":
        k = "name"
        v = "valueFrom"
    elif object_type == "tags":
        k = "key"
        v = "value"
    else:
        raise ValueError

    object_list = []
    for obj in objs.items():
        object_list.append({k: obj[0], v: str(obj[1])})

    return object_list


def expire_parameter_in_one_hour():
    """
    Expiration policies need to be passed as a stringified JSON array of expiration policies
    """
    one_hour_from_now = datetime.utcnow() + timedelta(hours=1)
    one_hour_from_now_string = one_hour_from_now.strftime("%Y-%m-%dT%H:%M:%SZ")
    # See https://docs.aws.amazon.com/systems-manager/latest/userguide/parameter-store-policies.html
    expiration_policy = [
        {"Type": "Expiration", "Version": "1.0", "Attributes": {"Timestamp": f"{one_hour_from_now_string}"}}
    ]
    return json.dumps(expiration_policy)


def build_docker_command(script):
    """
    Takes a list of script commands and prefixes them with -c so that
    Docker can read the commands from the command_string.
    """
    return ["-c", " && ".join(script)]
