#!/usr/bin/env python3
import yaml

from ecs_executor import RunnerJob


def parse_jobs(job_yaml):
    """Allows us to define jobs in a convenient .gitlab-ci.yml format for testing the ECS executor"""
    jobs_dict = yaml.safe_load(job_yaml)
    jobs = []
    for job_dict in jobs_dict:
        before_script_commands = jobs_dict[job_dict].get("before_script")
        script_commands = jobs_dict[job_dict]["script"]
        if before_script_commands:
            job_commands = before_script_commands + script_commands
        else:
            job_commands = script_commands
        job = {
            "name": job_dict,
            "image": jobs_dict[job_dict]["image"],
            "script_commands": job_commands,
        }
        variables = jobs_dict[job_dict].get("variables")
        if variables:
            job["variables"] = variables
        jobs.append(job)

    return jobs


if __name__ == "__main__":
    with open("runner-config.yml") as f:
        runner_config = yaml.safe_load(f.read())

    with open("jobs.yml") as f:
        contents = f.read()
    jobs = parse_jobs(contents)

    # TODO: This should be generated on the fly and passed in but we're just hard-coding them for now for demonstration
    job_predefined_variables = {
        "CI": True,
        "CI_JOB_ID": "50",
        "CI_COMMIT_SHA": "1ecfd275763eff1d6b4844ea3168962458c9f27a",
        "CI_COMMIT_SHORT_SHA": "1ecfd275",
        "CI_COMMIT_REF_NAME": "master",
        "CI_REPOSITORY_URL": "https://gitlab-ci-token:abcde-1234ABCD5678ef@example.com/gitlab-org/gitlab-foss.git",
        "CI_COMMIT_TAG": "1.0.0",
        "CI_JOB_NAME": "spec:other",
        "CI_JOB_STAGE": "test",
        "CI_JOB_MANUAL": "true",
        "CI_JOB_TRIGGERED": "true",
        "CI_PROJECT_ID": "34",
        "CI_PROJECT_DIR": "/builds/gitlab-org/gitlab-foss",
        "CI_PROJECT_NAME": "gitlab-foss",
        "CI_PROJECT_TITLE": "GitLab FOSS",
        "CI_PROJECT_NAMESPACE": "gitlab-org",
        "CI_PROJECT_ROOT_NAMESPACE": "gitlab-org",
        "CI_PROJECT_PATH": "gitlab-org/gitlab-foss",
        "CI_SERVER_URL": "https://example.com",
        "CI_JOB_URL": "https://example.com/gitlab-org/gitlab-foss/-/jobs/890862",
    }
    job_predefined_secrets = {
        "CI_JOB_TOKEN": "abcde-1234ABCD5678ef",
        "PROJECT_VARIABLE": "This is a project secret and shouldn't be able to be read from the task definition",
    }

    for job in jobs:
        # Merge the predefined CI and group/project variables with the variables from the .gitlab-ci.yml file
        if "variables" in job:
            job["variables"] = {**job_predefined_variables, **job["variables"]}
        else:
            job["variables"] = job_predefined_variables
        job["secrets"] = job_predefined_secrets
        job_run = RunnerJob(runner_config, job)
        job_run.run_job()
        for log_line in job_run.get_logs():
            print(log_line)
        status = job_run.get_job_status()
        if status == 0:
            print("Job succeeded")
        else:
            print(f"ERROR: Job failed: command terminated with exit code {status}")
